from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.exceptions import ValidationError
from . import models

def index(request):
    return render(request, 'landing.html', {'statuses': models.Status.objects.all()})

def submit(request):
    try:
        text = request.POST['text']
        assert (text != '')
        models.Status(text = text).save()
    except (KeyError, ValidationError, AssertionError, OverflowError):
        return render(request, 'landing.html', {'statuses': models.Status.objects.all()})
    return HttpResponseRedirect(reverse('index'))
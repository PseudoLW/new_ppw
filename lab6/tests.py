from django.test import TestCase, Client
from django.urls import resolve
from . import views, models

# Create your tests here.
class Lab6Test(TestCase):
    def test_urlExists(self):
        reponse = Client().get('/lab-6/')
        self.assertEqual(reponse.status_code, 200)

    def test_viewsFunction(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, views.index)

    def test_templateTest(self):
        reponse = Client().get('/lab-6/')
        self.assertTemplateUsed(reponse, 'landing.html')

    def test_modelCreation(self):
        new_activity = models.Status.objects.create(text='<A test Status>')
        counting_all_available_message = models.Status.objects.all().count()
        self.assertEqual(counting_all_available_message, 1)

    def test_postRequest(self):
        response = self.client.post('/lab-6/submit', data = {'text': 'Hello, world'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/lab-6/')
        newResponse = self.client.get('/lab-6/')
        htmlResponse = newResponse.content.decode('utf8')
        self.assertIn('Hello, world', htmlResponse)

    def test_postRequestEmptyData(self):
        response = self.client.post('/lab-6/submit', data = {'text': ''})
        self.assertEqual(response.status_code, 200)
